//
//  APIHelper.swift
//  GojekTest
//
//  Created by Christopher Sonny on 28/08/19.
//  Copyright © 2019 Christopher Sonny. All rights reserved.
//

import UIKit
import Alamofire

@objc protocol contactDelegate {
    @objc optional func getContactsDelegate(result:NSArray)
    @objc optional func getDetailContact(result:NSDictionary)
    @objc optional func editProfile()
    @objc optional func addContact()
    @objc optional func editFavorite()
}
class APIHelper : NSObject{
    static let shared = APIHelper()
    var delegate:contactDelegate?
    func getContacts(){
        Alamofire.request("http://gojek-contacts-app.herokuapp.com/contacts.json").responseJSON{
            response in
            let result = response.result.value as! NSArray
            self.delegate?.getContactsDelegate!(result: result)
        }
        
    }
    func getContactsDetail(id:Int){
        Alamofire.request("http://gojek-contacts-app.herokuapp.com/contacts/\(id).json").responseJSON{
            response in
          let result = response.result.value as! NSDictionary
            self.delegate?.getDetailContact?(result: result)
            
        }
        
    }
    func addContact(contact:Contact){
        let date = Date()
        let formatter = DateFormatter()
        let result = formatter.string(from: date)
        let parameters: [String: Any] = [
            "first_name": "\(contact.first_name)",
            "last_name": "\(contact.last_name)",
            "email": "\(contact.email)",
            "phone_number": "\(contact.mobile)",
            "profile_pic": "/images/missing.png",
            "favorite": false,
            "created_at": "2019-08-29T07:30:35.475Z",
            "updated_at": result
        ]
        
        Alamofire.request("http://gojek-contacts-app.herokuapp.com/contacts.json", method: .post, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                self.delegate?.addContact?()
        }
    }
    
    func editContact(contact:Contact){
        let date = Date()
        let formatter = DateFormatter()
        let result = formatter.string(from: date)
        let parameters: [String: Any] = [
            "first_name": "\(contact.first_name)",
            "last_name": "\(contact.last_name)",
            "email": "\(contact.email)",
            "phone_number": "\(contact.mobile)",
            "profile_pic": "/images/missing.png",
            "favorite": false,
            "created_at": "2019-08-29T07:30:35.475Z",
            "updated_at": result
        ]
        
        Alamofire.request("http://gojek-contacts-app.herokuapp.com/contacts/\(contact.id).json", method: .put, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                self.delegate?.editProfile?()
        }
    }
    
    func editContactFavorite(contact:Contact){
        let date = Date()
        let formatter = DateFormatter()
        let result = formatter.string(from: date)
        let parameters: [String: Any] = [
            "first_name": "\(contact.first_name)",
            "last_name": "\(contact.last_name)",
            "email": "\(contact.email)",
            "phone_number": "\(contact.mobile)",
            "profile_pic": "/images/missing.png",
            "favorite": contact.favorite,
            "created_at": "2019-08-29T07:30:35.475Z",
            "updated_at": result
        ]
        
        Alamofire.request("http://gojek-contacts-app.herokuapp.com/contacts/\(contact.id).json", method: .put, parameters: parameters, encoding: JSONEncoding.default)
            .responseJSON { response in
                self.delegate?.editFavorite?()
        }
    }
    
}
