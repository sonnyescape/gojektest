

import UIKit

class Contact: NSObject {
    var id:Int=0
    var first_name:String=""
    var last_name:String=""
    var profile_pic:String=""
    var favorite:Bool=false
    var url:String=""
    var mobile:String=""
    var email:String=""
}
