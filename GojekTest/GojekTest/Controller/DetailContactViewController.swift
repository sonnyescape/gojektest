//
//  DetailContactViewController.swift
//  GojekTest
//
//  Created by Christopher Sonny on 28/08/19.
//  Copyright © 2019 Christopher Sonny. All rights reserved.
//

import UIKit
import MessageUI
import PKHUD
class DetailContactViewController: UIViewController, contactDelegate, MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate {
    func editFavorite() {
        var text = ""
        if(contact.favorite==false){
            text="Favorite removed !"
        }else{
            text="Favorite added !"
        }
        let hud = PKHUDSuccessView(title: "", subtitle: text)
        PKHUD.sharedHUD.contentView=hud
        PKHUD.sharedHUD.show()
        PKHUD.sharedHUD.hide(afterDelay: 1.0)
        btnFavorite.isEnabled=true
        
    }
    @IBOutlet weak var btnFavorite: UIButton!
    @IBAction func btnFavorite(_ sender: Any) {
        if(contact.favorite == true){
            contact.favorite=false
            btnFavorite.setBackgroundImage(UIImage.init(named: "favourite_button"), for: .normal)
        }else
            if(contact.favorite == false){
                contact.favorite=true
                btnFavorite.setBackgroundImage(UIImage.init(named: "favourite_button_selected"), for: .normal)
        }
        APIHelper.shared.editContactFavorite(contact: contact)
        btnFavorite.isEnabled=false
    }
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch (result) {
        case .cancelled:
            print("Message was cancelled")
            dismiss(animated: true, completion: nil)
        case .failed:
            print("Message failed")
            dismiss(animated: true, completion: nil)
        case .sent:
            print("Message was sent")
            dismiss(animated: true, completion: nil)
        default:
            break
        }
    }
    
    func getDetailContact(result: NSDictionary) {
        contact.mobile=result["phone_number"] as! String
        contact.email=result["email"] as! String
        lblMobile.text=result["phone_number"] as! String
        lblEmail.text=result["email"] as! String
    }
    var gradientLayer: CAGradientLayer!
    @IBOutlet weak var viewBAse: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func btnDelete(_ sender: Any) {
    }
    var contact = Contact()
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       APIHelper.shared.getContactsDetail(id: contact.id)
        APIHelper.shared.delegate=self
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        navigationItem.leftBarButtonItem?.tintColor = UIColor.init(rgb: 0x50E3C2)
        var barbutton:UIBarButtonItem? = nil
        barbutton=UIBarButtonItem(title: "Edit", style: .plain, target: self, action:  #selector(editTapped))
        barbutton!.tintColor = UIColor.init(rgb: 0x50E3C2)
        navigationItem.rightBarButtonItem=barbutton
        imageProfile.downloaded(from: "http://gojek-contacts-app.herokuapp.com/"+contact.profile_pic)
        imageProfile.downloaded(from: contact.profile_pic)
        lblName.text=contact.first_name+" "+contact.last_name
        imageProfile.layer.cornerRadius=60
        if(contact.favorite != true){
            btnFavorite.setBackgroundImage(UIImage.init(named: "favourite_button"), for: .normal)
        }
    }
    @objc func editTapped(){
        let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:
            "editadd") as! EditAddViewController
        popvc.contact=contact
        popvc.type="edit"
        navigationController?.pushViewController(popvc, animated: true)
    }
    @IBAction func btnMessage(_ sender: Any) {
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.recipients = [lblMobile.text ?? ""]
        composeVC.body = ""
        
        // Present the view controller modally.
        if MFMessageComposeViewController.canSendText() {
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    @IBAction func btnCall(_ sender: Any) {
        if(lblMobile.text != ""){
        let url: NSURL = URL(string: "TEL://"+lblMobile.text!)! as NSURL
            UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        }
        
    }
    @IBAction func btnEmail(_ sender: Any) {
        sendEmail()
    }
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([lblEmail.text!])
            mail.setMessageBody("", isHTML: true)
            if(lblEmail.text != ""){
            present(mail, animated: true)
            }
        } else {
            // show failure alert
        }
    }
    @IBAction func btnFavourite(_ sender: Any) {
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    override func viewDidLayoutSubviews() {
        createGradientLayer()
    }
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = viewBAse.bounds
        let color2 = UIColor.init(rgb: 0x50E3C2).withAlphaComponent(0.28)
        
        gradientLayer.colors = [UIColor.white.cgColor, color2.cgColor]
        
        self.viewBAse.layer.insertSublayer(gradientLayer, at: 0)
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
