//
//  EditAddViewController.swift
//  GojekTest
//
//  Created by Christopher Sonny on 29/08/19.
//  Copyright © 2019 Christopher Sonny. All rights reserved.
//

import UIKit
import PKHUD
class EditAddViewController: UIViewController, contactDelegate {
    func editProfile() {
        PKHUD.sharedHUD.hide()
            navigationController?.popViewController(animated: true)
    }
    func addContact() {
       PKHUD.sharedHUD.hide()
        navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var viewBase: UIView!
    @IBOutlet weak var txtFirstNam: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    var contact = Contact()
    var type = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        var barbutton:UIBarButtonItem? = nil
        
        barbutton=UIBarButtonItem(title: "Done", style: .plain, target: self, action:  #selector(editAdd))
        barbutton!.tintColor = UIColor.init(rgb: 0x50E3C2)
        navigationItem.rightBarButtonItem=barbutton
        if(type=="edit"){
        imageProfile.downloaded(from: "http://gojek-contacts-app.herokuapp.com/"+contact.profile_pic)
        imageProfile.downloaded(from: contact.profile_pic)
        txtFirstNam.text=contact.first_name
        txtLastName.text=contact.last_name
        txtMobile.text=contact.mobile
        txtEmail.text=contact.email
        }
    }
    @objc func editAdd(){
        
        contact.first_name=txtFirstNam.text ?? ""
        contact.email=txtEmail.text ?? ""
        contact.last_name=txtLastName.text ?? ""
        contact.mobile=txtMobile.text ?? ""
        APIHelper.shared.delegate=self
        if(type=="edit"){
        APIHelper.shared.editContact(contact: contact)
            
        }else{
            APIHelper.shared.addContact(contact: contact)
        }
        let hud = PKHUDProgressView(title: "Please Wait", subtitle: "Processing your request")
        PKHUD.sharedHUD.contentView=hud
        PKHUD.sharedHUD.show()
    }
    override func viewDidLayoutSubviews() {
        createGradientLayer()
    }
    var gradientLayer: CAGradientLayer!
    func createGradientLayer() {
        gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = viewBase.bounds
        let color2 = UIColor.init(rgb: 0x50E3C2).withAlphaComponent(0.28)
        
        gradientLayer.colors = [UIColor.white.cgColor, color2.cgColor]
        
        self.viewBase.layer.insertSublayer(gradientLayer, at: 0)
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
