//
//  ViewController.swift
//  GojekTest
//
//  Created by Christopher Sonny on 28/08/19.
//  Copyright © 2019 Christopher Sonny. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, contactDelegate {
    var sections = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
    func getContactsDelegate(result: NSArray) {
        var index=0
        var tempContact = [Contact]()
        for i in result{
            let results = i as! NSDictionary
            let contact = Contact()
            contact.favorite=results["favorite"] as! Bool
            contact.first_name=results["first_name"] as! String
            contact.last_name=results["last_name"] as! String
            contact.id=results["id"] as! Int
            contact.profile_pic=results["profile_pic"] as! String
            contact.url=results["url"] as! String
            tempContact.append(contact)
        }
        tempContact = tempContact.sorted(by: {
            $0.first_name.lowercased().compare($1.first_name.lowercased()) == .orderedAscending
        })
        var sortedContact = [Contact]()
        twoArrContacts.removeAll()
        var flag = false
        for i in tempContact{
            for j in sections{
                if(j==String(i.first_name.first!).lowercased()){
                    flag=true
                }
            }
            if(flag==true){
            if(String(i.first_name.first!).lowercased() == sections[index])
            {
                            sortedContact.append(i)
                        }else{
                            index=index+1
                            twoArrContacts.append(sortedContact)
                            sortedContact.removeAll()
                        }
            }
            
        }
        contactTableView.reloadData()
        contactTableView.allowsSelection=true
    }
    var twoArrContacts = [[Contact]()]
    var arrContactsFavorite = [Contact]()
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let label = UILabel()
        label.text=sections[section].uppercased()
        label.backgroundColor=UIColor.init(rgb: 0xE7E7E7)
        //label.safeAreaInsets=UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0)
        return label
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 28
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return twoArrContacts.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return twoArrContacts[section].count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:
            "detailcontact") as! DetailContactViewController
        popvc.contact=twoArrContacts[indexPath.section][indexPath.row]
        navigationController?.pushViewController(popvc, animated: true)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = contactTableView.dequeueReusableCell(withIdentifier: "cell") as! ContactsTableViewCell
        cell.nameProfile.text=twoArrContacts[indexPath.section][indexPath.row].first_name+" "+twoArrContacts[indexPath.section][indexPath.row].last_name
        cell.imageProfile.downloaded(from: "http://gojek-contacts-app.herokuapp.com/"+twoArrContacts[indexPath.section][indexPath.row].profile_pic)
        cell.imageProfile.downloaded(from: twoArrContacts[indexPath.section][indexPath.row].profile_pic)
       cell.imageProfile.layer.cornerRadius=20
        if(twoArrContacts[indexPath.section][indexPath.row].favorite==true){
            cell.favoriteImage.isHidden=false
            cell.favoriteImage.image=UIImage(named: "home_favourite")
        }else{
            cell.favoriteImage.isHidden=true
        }
        return cell
    }
    
    var arrContacts = [Contact]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var contactTableView: UITableView!
    var barbutton:UIBarButtonItem? = nil
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        contactTableView.allowsSelection=false
        APIHelper.shared.getContacts()
        APIHelper.shared.delegate=self
        navigationItem.title="Contacts"
        navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: UIColor.black,
             NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17, weight: UIFont.Weight.semibold)]
        barbutton=UIBarButtonItem(title: "+", style: .plain, target: self, action:  #selector(plusTapped))
        barbutton!.tintColor = UIColor.init(rgb: 0x50E3C2)
        navigationItem.rightBarButtonItem=barbutton
        barbutton=UIBarButtonItem(title: "Groups", style: .plain, target: self, action:  #selector(groupTapped))
        barbutton!.tintColor = UIColor.init(rgb: 0x50E3C2)
        navigationItem.leftBarButtonItem=barbutton
        contactTableView.delegate=self
        contactTableView.dataSource=self
        contactTableView.register(UINib(nibName: "ContactsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    }
    
    @objc func plusTapped(){
        let popvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:
            "editadd") as! EditAddViewController
        popvc.type="add"
        navigationController?.pushViewController(popvc, animated: true)
    }
    @objc func groupTapped(){
        
    }
}
extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    
    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
